% *Exodus II Exporter for Haskell*
% 12 December, 2018
% Patrick Suggate

\clearpage

# Package: exodus #

Haskell bindings to the Exodus II library, and a simple wrapper that features a stateful exporter.

## TODO ##

 + type-level parameters for the exporter's state-monad, `ExIIstate`;
 + documentation;
 + testbenches & examples;

\clearpage

# Example: Quadtree Export #

Exports a linear quadtree that is represented using an `IntSet`. First:

```bash
$ sudo aptitude install libexodusii-dev
```

to install the Exodus II library & headers.
