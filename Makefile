# Source Markdown files:
MD	:= $(wildcard *.md)

# Include-files:
INC	:= $(filter %.inc.md, $(MD))

# Images:
PNG	:= $(wildcard doc/images/*.png)
SVG	:= $(wildcard *.svg)
PIC	:= $(SVG:.svg=.pdf)

# Top-level documents:
DOC	:= $(filter-out %.inc.md, $(MD))
PDF	:= $(DOC:.md=.pdf)

# Pandoc settings:
FLT	?= --filter=pandoc-include --filter=pandoc-fignos --filter=pandoc-citeproc --filter=pandoc-filter-graphviz
OPT	?= --number-sections --toc --toc-depth=2 -V toccolor:purple -V fontsize:11pt -V papersize:a4 -V geometry:margin=2cm
SRC	?= markdown+tex_math_double_backslash


#
#  Targets
# -------------------------------------------------------------------------- #
.PHONY:			all clean
all:	doc

doc:	$(PDF) $(PIC)

clean:
		rm -f $(PDF) $(PIC)

#
#  Implicit rules
# -------------------------------------------------------------------------- #
%.pdf: %.md $(PIC) $(PNG) $(TMP) $(INC)
	+pandoc --template=$(TMP) $(FLT) $(OPT) -f $(SRC) -t latex -V papersize:a4 $< -o $@

%.pdf: %.md $(PIC)
	+pandoc $(OPT) -f $(SRC) -t latex $< -o $@

%.pdf: %.svg
		+inkscape --export-area-drawing --export-text-to-path --export-pdf=$@ $<
