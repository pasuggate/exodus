{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE ForeignFunctionInterface #-}
{-# LANGUAGE OverloadedStrings #-}
module Main where

import Numeric.Formats.ExodusII
-- import Numeric.Formats.ExodusII.Inline


main = do
  putStrLn "Hello"
  testExo >>= print
  testBaseExo
