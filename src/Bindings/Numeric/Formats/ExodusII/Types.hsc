{-# LANGUAGE GeneralizedNewtypeDeriving, TemplateHaskell, DeriveGeneric,
             PatternSynonyms, TypeSynonymInstances, FlexibleInstances,
             OverloadedStrings
  #-}

------------------------------------------------------------------------------
-- |
-- Module      : Bindings.Numeric.Formats.ExodusII.Types
-- Copyright   : (C) Patrick Suggate 2020
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Re-exports and wrappers for Exodus II types.
-- 
-- Changelog:
--  + 28/01/2020  --  initial file;
-- 
-- TODO:
-- 
------------------------------------------------------------------------------

#include <exodusII.h>
#include <bindings.dsl.h>

module Bindings.Numeric.Formats.ExodusII.Types
  ( CanCString (..)
  , Exo (..)
  , ExET (..)
  , ExID (..)
  , C'ex_entity_id
  , C'ex_entity_type
  , C'ex_options
    -- Constants:
  , c'EX_CLOBBER
  , c'EX_API_VERS_NODOT
  , c'EX_ELEM_BLOCK
  , c'EX_NODE_SET
  ) where

#strict_import
import Foreign.C.String
import GHC.Generics (Generic)
import qualified Data.ByteString.Char8 as BS


-- * Exodus II native types.
------------------------------------------------------------------------------
-- | ExodusII version.
#num EX_API_VERS_NODOT

-- | File modes.
#num EX_NOCLOBBER
#num EX_CLOBBER
#num EX_NORMAL_MODEL
#num EX_LARGE_MODEL
#num EX_NETCDF4
#num EX_NOSHARE
#num EX_SHARE
#num EX_NOCLASSIC

------------------------------------------------------------------------------
-- | ExodusII entities.
#integral_t ex_entity_id

-- | Entity types.
#integral_t ex_entity_type
#num EX_NODAL
#num EX_NODE_BLOCK
#num EX_NODE_SET
#num EX_EDGE_BLOCK
#num EX_EDGE_SET
#num EX_FACE_BLOCK
#num EX_FACE_SET
#num EX_ELEM_BLOCK
#num EX_ELEM_SET
#num EX_SIDE_SET
#num EX_ELEM_MAP
#num EX_NODE_MAP
#num EX_EDGE_MAP
#num EX_FACE_MAP
#num EX_GLOBAL
#num EX_COORDINATE
#num EX_INVALID

------------------------------------------------------------------------------
-- | Exodus II runtime options.
#integral_t ex_options
#num EX_DEFAULT
#num EX_VERBOSE
#num EX_DEBUG
#num EX_ABORT
#num EX_NULLVERBOSE

-- ** Wrappers for some Exodus II types.
------------------------------------------------------------------------------
newtype Exo  = Exo  CInt
  deriving (Eq, Show, Storable, Generic)

newtype ExET = ExET C'ex_entity_type
  deriving (Eq, Ord, Enum, Show, Storable, Generic)

newtype ExID = ExID C'ex_entity_id
  deriving (Eq, Ord, Enum, Show, Storable, Generic)


-- * Make the functions polymorphic in the string-type.
------------------------------------------------------------------------------
class CanCString s where
  withString :: s -> (CString -> IO a) -> IO a
  toCString  :: s -> IO CString

  -- ^ defaults:
  withString s k = toCString s >>= k
  toCString  s   = withString s return


-- * Instances for string-conversions.
------------------------------------------------------------------------------
-- | Support `ByteString's.
instance CanCString (BS.ByteString) where
  withString s k = BS.useAsCString s k
  {-# INLINE withString #-}

-- | Support slow `String's.
instance CanCString String where
  withString = withCString
  toCString  = newCString
  {-# INLINE withString #-}
  {-# INLINE toCString #-}

-- | Unsafe, as `CString's are mutable.
instance CanCString CString where
  withString = flip ($)
  toCString  = return
  {-# INLINE withString #-}
  {-# INLINE toCString #-}
