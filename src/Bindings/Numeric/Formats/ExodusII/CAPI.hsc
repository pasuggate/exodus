{-# LANGUAGE CPP, ForeignFunctionInterface, GeneralizedNewtypeDeriving
  #-}

------------------------------------------------------------------------------
-- |
-- Module      : Bindings.Numeric.Formats.ExodusII.CAPI
-- Copyright   : (C) Patrick Suggate 2020
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Re-exports and wrappers for Exodus II functions.
-- 
------------------------------------------------------------------------------

#include <exodusII.h>
#include <bindings.dsl.h>

module Bindings.Numeric.Formats.ExodusII.CAPI where

#strict_import
import Bindings.Numeric.Formats.ExodusII.Types


-- * Exodus file handling.
------------------------------------------------------------------------------
-- #ccall ex_create     , CString -> CInt -> Ptr CInt -> Ptr CInt -> IO Exo
#ccall ex_create_int , CString -> CInt -> Ptr CInt -> Ptr CInt -> CInt -> IO Exo
#ccall ex_update     , Exo -> IO CInt
#ccall ex_close      , Exo -> IO CInt

-- * Initialisation.
------------------------------------------------------------------------------
#ccall ex_put_init , Exo -> CString -> CLong -> CLong -> CLong -> CLong -> CLong -> CLong -> IO CInt

-- * Set properties.
------------------------------------------------------------------------------
#ccall ex_put_prop        , Exo -> ExET -> ExID -> CString -> CInt -> IO CInt
#ccall ex_put_prop_names  , Exo -> ExET -> CInt -> Ptr CString -> IO CInt
#ccall ex_put_prop_array  , Exo -> ExET -> CString -> Ptr CInt -> IO CInt

-- * Set maps.
------------------------------------------------------------------------------
#ccall ex_put_map         , Exo -> Ptr CInt -> IO CInt
#ccall ex_put_map_param   , Exo -> CInt -> CInt -> IO CInt

-- * Set coordinates.
------------------------------------------------------------------------------
#ccall ex_put_coord       , Exo -> Ptr CDouble -> Ptr CDouble -> Ptr CDouble -> IO CInt
#ccall ex_put_coord_names , Exo -> Ptr CString -> IO CInt

-- * Set nodes.
------------------------------------------------------------------------------
#ccall ex_put_node_map           , Exo -> ExID -> IO CInt
#ccall ex_put_node_set           , Exo -> ExID -> Ptr CInt -> IO CInt
#ccall ex_put_node_set_dist_fact , Exo -> ExID -> Ptr () -> IO CInt
#ccall ex_put_node_set_param     , Exo -> ExID -> CLong -> CLong -> IO CInt

-- * Set elements
------------------------------------------------------------------------------
#ccall ex_put_elem_attr_names , Exo -> ExID -> Ptr CString -> IO CInt
#ccall ex_put_elem_attr       , Exo -> ExID -> Ptr () -> IO CInt
#ccall ex_put_elem_block      , Exo -> ExID -> CString -> CLong -> CLong -> CLong -> IO CInt
#ccall ex_put_elem_conn       , Exo -> ExID -> Ptr CInt -> IO CInt
#ccall ex_put_elem_map        , Exo -> ExID -> IO CInt
#ccall ex_put_elem_num_map    , Exo -> ExID -> Ptr () -> IO CInt
-- #ccall ex_put_elem_var        , Exo -> CInt -> CInt -> ExID -> CLong -> Ptr () -> IO CInt

-- * Set sides.
------------------------------------------------------------------------------
#ccall ex_put_side_set           , Exo -> ExID -> Ptr () -> Ptr () -> IO CInt
#ccall ex_put_side_set_dist_fact , Exo -> ExID -> Ptr () -> IO CInt
#ccall ex_put_side_set_param     , Exo -> ExID -> CLong -> CLong -> IO CInt
#ccall ex_put_sset_var           , Exo -> CInt -> CInt -> ExID -> CLong -> Ptr () -> IO CInt
#ccall ex_put_sset_var_tab       , Exo -> CInt -> CInt -> Ptr CInt -> IO CInt

-- * Set results variables.
------------------------------------------------------------------------------
#ccall ex_put_var_name  , Exo -> CString -> CInt -> CString -> IO CInt
#ccall ex_put_var_names , Exo -> CString -> CInt -> Ptr CString -> IO CInt
#ccall ex_put_var_param , Exo -> CString -> CInt -> IO CInt
#ccall ex_put_glob_vars , Exo -> CInt -> CInt -> Ptr () -> IO CInt
#ccall ex_put_nodal_var , Exo -> CInt -> CInt -> CLong -> Ptr () -> IO CInt
#ccall ex_put_elem_var  , Exo -> CInt -> CInt -> ExID -> CLong -> Ptr () -> IO CInt

-- * Set time-step times.
------------------------------------------------------------------------------
#ccall ex_put_time , Exo -> CInt -> Ptr () -> IO CInt
