
#include <stdio.h>

#include <math.h>

#include <exodusII.h>

int inline_c_Numeric_Formats_ExodusII_Inline_0_4b68ac022645a45895a70429e01080cd461776ef(char * fp_inline_c_0, int mode_inline_c_1, int * p_inline_c_2, int * p_inline_c_3) {
return ( ex_create(fp_inline_c_0, mode_inline_c_1, p_inline_c_2, p_inline_c_3) );
}


int inline_c_Numeric_Formats_ExodusII_Inline_1_ac05247710c75b5f50e9eb3415cf5bbb5fb475e8() {
return ( printf("Some number: %.2f\n", cos(0.5)) );
}

