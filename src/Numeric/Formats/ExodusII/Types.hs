{-# LANGUAGE GeneralizedNewtypeDeriving, TemplateHaskell, DeriveGeneric,
             PatternSynonyms, TypeSynonymInstances, FlexibleInstances,
             OverloadedStrings
  #-}

------------------------------------------------------------------------------
-- |
-- Module      : Numeric.Formats.ExodusII.Types
-- Copyright   : (C) Patrick Suggate 2020
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Re-exports and wrappers for Exodus II types.
-- 
------------------------------------------------------------------------------

module Numeric.Formats.ExodusII.Types
  ( -- Re-exports:
    Exo (..)
  , ExID (..)
  , ExET (..)
  , CanCString (..)
    -- Constants:
  , c'EX_CLOBBER
  , c'EX_API_VERS_NODOT
  , c'EX_ELEM_BLOCK
  , c'EX_NODE_SET
    -- Exodus II types:
  , ExoMode (..)
  , ExoDims (..)
  , ExoInit (..)
  , exNumDims
  , exNumNodes
  , exNumElems
  , exNumEBlks
  , exNumNSets
  , exNumSSets
  ) where

import GHC.Generics (Generic)
import Control.Lens (makeLenses)

import Bindings.Numeric.Formats.ExodusII.Types


-- * Wrappers for Exodus II types.
------------------------------------------------------------------------------
-- | Mode setting for floating-point precision.
data ExoMode
  = ExoCreateDouble
  | ExoCreateFloat
  | ExoCreateDoubleFloat
  deriving (Eq, Show, Generic)

------------------------------------------------------------------------------
-- | Number of spatial dimensions for the mesh.
data ExoDims
  = Exo1D
  | Exo2D
  | Exo3D
  deriving (Eq, Ord, Enum, Bounded, Read, Show, Generic)

------------------------------------------------------------------------------
-- | Initialisation parameters for a generated Exodus II file.
data ExoInit =
  ExoInit { _exNumDims  :: ExoDims -- ^ either 2D or 3D meshes are supported
          , _exNumNodes :: Int     -- ^ number of mesh vertices(?)
          , _exNumElems :: Int     -- ^ number of mesh cells/elements(?)
          , _exNumEBlks :: Int     -- ^ elements can be grouped into blocks(?)
          , _exNumNSets :: Int     -- ^ nodes can form separate node-sets(?)
          , _exNumSSets :: Int
          } deriving (Eq, Show, Generic)

makeLenses ''ExoInit


-- pattern EX_ELEM_BLOCK = ExET c'EX_ELEM_BLOCK
