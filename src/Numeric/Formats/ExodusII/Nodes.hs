{-# LANGUAGE CPP, ForeignFunctionInterface, TemplateHaskell, QuasiQuotes,
             OverloadedStrings, FlexibleInstances, FlexibleContexts,
             TypeSynonymInstances, GeneralizedNewtypeDeriving, DeriveGeneric,
             RankNTypes, GADTs, DefaultSignatures
  #-}

------------------------------------------------------------------------------
-- |
-- Module      : Numeric.Formats.ExodusII.Nodes
-- Copyright   : (C) Patrick Suggate 2016
-- License     : BSD
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Writes nodes and node-sets, for the Haskell ExodusII exporter.
-- 
-- Changelog:
--  + 12/09/2016  -- initial file;
--  + 02/02/2020  -- refactored the elements stuff into separate module;
-- 
------------------------------------------------------------------------------

module Numeric.Formats.ExodusII.Nodes
       ( ExIICoords   (..)
       , ExIINodes    (..)
       , ExIINodeSets (..)
       , NodesData    (..)
       , Ex2NodeSet   (..)
       , Nodes
       , Coords2D
       , Coords3D
       , V2R
       , V3R
       , vu3
       ) where

import Prelude hiding (id, (.))
import GHC.Generics (Generic)
import Foreign.Ptr
import Foreign.C.Types
import Foreign.C.String
import Foreign.Marshal.Utils (with)
import Foreign.Marshal.Array

import Control.Category
import Control.Monad (when, unless)
import Control.Monad.IO.Class
import Control.Monad.Trans.State
import Control.Lens (makeLenses, (^.), (%~), (.~))
import Data.Bool
import Data.Vector.Storable (Vector, Storable)
import qualified Data.Vector.Storable as Vec
import qualified Data.ByteString.Char8 as BS
import Linear
import Text.Printf
import Data.Text (pack)
import Control.Logging

import Numeric.Formats.ExodusII.Types
import Numeric.Formats.ExodusII.CAPI
import Numeric.Formats.ExodusII.Base


-- * ExodusII nodes writer classes.
------------------------------------------------------------------------------
-- | Store mesh-vertex coordinates.
class ExIICoords a where
  x2coords :: MonadIO m => a -> ExII m ()

-- ** Node-sets functions
------------------------------------------------------------------------------
-- | Extract node-sets information from the mesh data type.
class ExIINodeSets a where
  x2numsets  :: a -> Int
  x2nodesets :: a -> [Ex2NodeSet]
  x2nsprops  :: a -> [Label]
  x2nsprops   = const []   -- ^ default method, if no named properties

------------------------------------------------------------------------------
-- | Getters for the various node-set properties.
class NodesData a where
--   nodeSetId :: a -> Int
  numNodes  :: a -> Int
  nodesList :: a -> Vector CInt
  distFacts :: a -> Vector Double
  nodeIdMap :: a -> Vector CInt
  setProps  :: a -> [Label]

------------------------------------------------------------------------------
-- | Write node sets.
class ExIINodes a where
  x2nodes :: MonadIO m => a -> ExII m ()
  default x2nodes :: (MonadIO m, ExIINodeSets a) => a -> ExII m ()
  x2nodes mesh = ExII $ do
    exo <- _ex2file <$> get
    let ns = [ExID 1..] `zip` x2nodesets mesh
        ps = x2nsprops mesh
        en = toEnum c'EX_NODE_SET
    -- do the element-blocks have named properties?
    unless (null ps) $ liftIO $ do
      dbg "writing property-names"
      ex_put_prop_names exo en ps >> pure ()
    -- write each node-set:
    mapM_ `flip` ns $ \(i, Ex2NodeSet s) -> do
      dbg $ printf "writing node-set: %s" (show i)
      let df = distFacts s
      liftIO $ do
        dbg $ printf "writing nodes: %d (%s)" (numNodes s) (show . Vec.toList $ nodesList s)
        ex_put_node_set_param exo i (numNodes s + 1) (Vec.length df)
        ex_put_node_set exo i (nodesList s)
        when (Vec.length df > 0) $ do
          dbg "writing distribution-factors"
          ex_put_node_set_dist_fact exo i df >> pure ()
        (\p -> ex_put_prop exo en i p 1) `mapM_` setProps s
      modify $ ex2vals %~ (exNumNSets %~ pred)


-- * Data-types for exporting to ExodusII files
------------------------------------------------------------------------------
-- | Node-set wrapper so that generic instances can be used for writing nodes
--   data.
data Ex2NodeSet where
  Ex2NodeSet :: forall a. NodesData a => a -> Ex2NodeSet

-- ** Convenience aliases
------------------------------------------------------------------------------
type Nodes = Vector CInt

type Coords2D = Vector V2R
type Coords3D = Vector V3R

type V2R   = V2 Double
type V3R   = V3 Double


-- * ExodusII mesh-writer instances.
------------------------------------------------------------------------------
-- | Write 2D coordinates.
--   TODO: Number of dimensions is 2, but three coord-labels given.
instance ExIICoords (Vector V2R) where
  x2coords vs = ExII $ do
    st  <- get
    if nnodes st == Vec.length vs && ndims st == 2
      then (do let exo = st^.ex2file
                   st' = ex2vals %~ (exNumNodes .~ 0) $ st
               liftIO $ do
                 -- ^ Set the coordinates:
                 ex_put_coord_2d exo vs
                 ex_put_coord_names exo "xcoor" "ycoor" ("zcoor" :: BS.ByteString)
               -- ^ Update the state:
               put st' >> return ())
      else let err = printf (errMsgs!!0) (Vec.length vs) (nnodes st)
           in  s2tell $ Just $ BS.pack err

------------------------------------------------------------------------------
-- | Write 3D coordinates.
instance ExIICoords (Vector V3R) where
  x2coords vs = ExII $ do
    st  <- get
    if nnodes st == Vec.length vs && ndims st == 3
      then (do let exo = st^.ex2file
                   st' = ex2vals %~ (exNumNodes .~ 0) $ st
               liftIO $ do
                 -- ^ Set the coordinates:
                 ex_put_coord_3d exo vs
                 ex_put_coord_names exo "xcoor" "ycoor" ("zcoor" :: BS.ByteString)
               -- ^ Update the state:
               put st' >> return ())
      else let err = printf (errMsgs!!0) (Vec.length vs) (nnodes st)
           in  s2tell $ Just $ BS.pack err

------------------------------------------------------------------------------
-- | Write node sets.
--   
--   TODO: Support adding the node-set ID's to each element?
--   TODO: Distribution factors?
instance ExIINodes [Nodes] where
  x2nodes ns = x2fail () $ ExII $ do
    st <- get
    let (nn, nb) = (sum $ Vec.length <$> ns, length ns)
    if nnodes st == 0 && nnsets st >= nb
      then (do let exo = st^.ex2file
                   st' = ex2vals %~ (exNumNSets %~ (\x -> x - nb)) $ st
--                    nt  = toEnum c'EX_NODE_SET
                   nz  = [ExID 1..] `zip` ns
               liftIO $ do
                 -- ^ Set up the node sets:
                 flip mapM_ nz $ \(i, n) -> do
                   ex_put_node_set_param exo i (Vec.length n) 0
                   ex_put_node_set       exo i n
--                    ex_put_node_set_dist_fact exo i d -- TODO: distribution factors?
               -- ^ Update the state:
               put st' >> return ())
      else s2tell $ Just (BS.pack $ errMsgs!!1)


-- * Helper functions
------------------------------------------------------------------------------
vu3 :: Storable a => Vector (V3 a) -> [Vector a]
vu3 vs = let vx = Vec.map (^._x) vs
             vy = Vec.map (^._y) vs
             vz = Vec.map (^._z) vs
         in  [vx, vy, vz]
