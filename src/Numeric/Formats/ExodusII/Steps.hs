{-# LANGUAGE CPP, ForeignFunctionInterface, TemplateHaskell, QuasiQuotes,
             OverloadedStrings, FlexibleInstances, FlexibleContexts,
             TypeSynonymInstances, GeneralizedNewtypeDeriving, DeriveGeneric,
             RankNTypes, GADTs, DefaultSignatures
  #-}

------------------------------------------------------------------------------
-- |
-- Module      : Numeric.Formats.ExodusII.Steps
-- Copyright   : (C) Patrick Suggate 2016
-- License     : BSD
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Writes node- and element- data that is attached to the mesh, and for each
-- time-step.
-- 
-- Changelog:
--  + 12/09/2016  -- initial file;
--  + 02/02/2020  -- refactored the elements stuff into separate module;
-- 
------------------------------------------------------------------------------

module Numeric.Formats.ExodusII.Steps
       ( ExIITSteps   (..)
       , ExIIResults  (..)
       , ExIIGlobals  (..)
       , ExIINData    (..)
       , ExIIEData    (..)
       , TimeStep    (..)
       , Ex2TStep     (..)
       ) where

import Prelude hiding (id, (.))
import GHC.Generics (Generic)
import Foreign.Ptr
import Foreign.C.Types
import Foreign.C.String
import Foreign.Marshal.Utils (with)
import Foreign.Marshal.Array

import Control.Category
import Control.Monad (when, unless)
import Control.Monad.IO.Class
import Control.Monad.Trans.State
import Control.Lens (makeLenses, (^.), (%~), (.~))
import Data.Bool
import Data.Vector.Storable (Vector, Storable)
import qualified Data.Vector.Storable as Vec
import qualified Data.ByteString.Char8 as BS
import Linear
import Text.Printf

import Numeric.Formats.ExodusII.Types
import Numeric.Formats.ExodusII.CAPI
import Numeric.Formats.ExodusII.Base


-- * ExodusII results-data writer classes.
------------------------------------------------------------------------------
-- | Write the results-data for each time-step.
class ExIITSteps a where
  x2tsteps :: MonadIO m => a -> ExII m ()
  default x2tsteps :: (MonadIO m, ExIIResults a) => a -> ExII m ()
  x2tsteps mesh = ExII $ do
    exo <- _ex2file <$> get
    -- write the numbers of, and labels for, each type of results-data
    let (gl, gn) = (x2glabels mesh, length gl)
        (nl, nn) = (x2nlabels mesh, length nl)
        (el, en) = (x2elabels mesh, length el)
    liftIO $ do
      ex_put_var_param exo ("G" :: BS.ByteString) gn
      ex_put_var_names exo ("G" :: BS.ByteString) gn gl
      ex_put_var_param exo ("N" :: BS.ByteString) nn
      ex_put_var_names exo ("N" :: BS.ByteString) nn nl
      ex_put_var_param exo ("E" :: BS.ByteString) en
      ex_put_var_names exo ("E" :: BS.ByteString) en el

    -- step through the set of results, and writing each type
    liftIO $ mapM_ `flip` x2results mesh $ \(Ex2TStep x) -> do
      -- write time-step number, and tile-value:
      let c = stepCount x
          t = timeStep  x
      ex_put_time exo c t

      -- write the global, nodal, and elemental variables:
      unless (gn == 0) $ do
        ex_put_glob_vars exo c (globalsOf x) >> pure ()

      -- write nodal variables (1st var for each node, then the 2nd, ...):
      unless (nn == 0) $ do
        let ns = [1..] `zip` nodesData x
        uncurry (ex_put_nodal_var exo c) `mapM_` ns

      -- write elemental variables, and for each element-block:
      -- NOTE: outer-index is for each element variable, the next is for the
      --   element-block, and the inner-most index is for each element of a
      --   block.
      unless (en == 0) $ do
        let es = [1..] `zip` elemsData x
            bs = eblockIds x
        mapM_ `flip` es $ \(i, xs) -> do
          uncurry (ex_put_elem_var exo c i) `mapM_` zip bs xs

    pure ()

-- ** Batch-writers by results-data type
------------------------------------------------------------------------------
-- | Write just global results data.
class ExIIGlobals a where
  x2globs :: MonadIO m => a -> ExII m ()

-- | Write just nodal results data.
class ExIINData a where
  x2ndata :: MonadIO m => a -> ExII m ()

------------------------------------------------------------------------------
-- | Write just element results data, and by block ID.
class ExIIEData a where
  x2edata :: MonadIO m => a -> ExII m ()

-- ** Node-sets functions
------------------------------------------------------------------------------
-- | Extract node-sets information from the mesh data type.
class ExIIResults a where
  x2glabels :: a -> [Label]
  x2nlabels :: a -> [Label]
  x2elabels :: a -> [Label]
  x2results :: a -> [Ex2TStep]

------------------------------------------------------------------------------
-- | Getters for the various node-set properties.
class TimeStep a where
  timeStep  :: a -> Double
  stepCount :: a -> Int
  globalsOf :: a -> Vector Double
  nodesData :: a -> [Vector Double]
  eblockIds :: a -> [ExID]
  elemsData :: a -> [[Vector Double]]


-- * Data-types for exporting to ExodusII files
------------------------------------------------------------------------------
-- | Node-set wrapper so that generic instances can be used for writing time-
--   step data.
data Ex2TStep where
  Ex2TStep :: forall a. TimeStep a => a -> Ex2TStep

{-- }
-- ** Sub data-types for exporting to ExodusII files
------------------------------------------------------------------------------
-- | Represents a single, nodal data-set; e.g., pressure, so that multiple
--   sets can be extracted and stored in a list.
data Ex2NData where
  Ex2NData :: forall a. (ExIINData a, ExIILabels a) => a -> Ex2NData
--}


-- * ExodusII mesh-writer instances.
------------------------------------------------------------------------------
-- | Write nodal results-data.
instance ExIINData [(ExID, Vector Double)] where
  x2ndata xs = x2fail () $ ExII $ do
    st <- get
    if nnodes st == 0 && nelems st == 0 && st^.ex2time > 0
      then (do let exo = st^.ex2file
               t <- unExII x2gettime
               -- ^ Set the nodal variables:
               liftIO $ flip mapM_ xs $ \(i, n) -> do
                 ex_put_nodal_var exo t (fromEnum i) n
               return ())
      else s2tell $ Just (BS.pack $ errMsgs!!1)

-- | Write element results-data, and by block ID.
instance ExIIEData (ExID, [(ExID, Vector Double)]) where
  x2edata (b, xs) = x2fail () $ ExII $ do
    st <- get
    if nnodes st == 0 && nelems st == 0 && st^.ex2time > 0
      then (do let exo = st^.ex2file
               t <- unExII x2gettime
               -- ^ Set the nodal variables:
               liftIO $ flip mapM_ xs $ \(i, n) -> do
                 ex_put_elem_var exo t (fromEnum i) b n
               return ())
      else s2tell $ Just (BS.pack $ errMsgs!!1)
