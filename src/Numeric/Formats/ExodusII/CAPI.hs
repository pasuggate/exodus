{-# LANGUAGE CPP, ForeignFunctionInterface, TemplateHaskell, QuasiQuotes,
             OverloadedStrings
  #-}

------------------------------------------------------------------------------
-- |
-- Module      : Numeric.Formats.ExodusII.CAPI
-- Copyright   : (C) Patrick Suggate 2020
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Haskell bindings of the Exodus II C API functions.
-- 
------------------------------------------------------------------------------

module Numeric.Formats.ExodusII.CAPI
       ( module Numeric.Formats.ExodusII.CAPI
       , module Numeric.Formats.ExodusII.Types
       ) where

import Foreign.Ptr
import Foreign.C.Types
import Foreign.C.String
import Foreign.Marshal.Utils (with)
import Foreign.Marshal.Array

import Control.Lens (makeLenses, (^.))
import Data.Vector.Storable (Vector, Storable)
import qualified Data.Vector.Storable as Vec
import qualified Data.ByteString.Char8 as BS
import Linear

import Numeric.Formats.ExodusII.Types
import Bindings.Numeric.Formats.ExodusII.CAPI


-- * Exodus file operations.
------------------------------------------------------------------------------
-- | Create a new ExodusII file.
ex_create :: CanCString s => s -> ExoMode -> IO Exo
ex_create fp em = do
  let (mode, cpu, io) = case em of
        ExoCreateDouble      -> (c'EX_CLOBBER, 8, 8)
        ExoCreateFloat       -> (c'EX_CLOBBER, 4, 4)
        ExoCreateDoubleFloat -> (c'EX_CLOBBER, 8, 4)
  str <- toCString fp
  with cpu $ \c -> do
    with io $ \p -> c'ex_create_int str mode c p c'EX_API_VERS_NODOT

-- | Flush any pending changes to the file.
ex_update :: Exo -> IO CInt
ex_update  = c'ex_update

-- | Close the given ExodusII file.
ex_close :: Exo -> IO CInt
ex_close  = c'ex_close


-- * Exodus settings.
------------------------------------------------------------------------------
ex_init :: CanCString s => Exo -> s -> ExoInit -> IO CInt
ex_init exo nam (ExoInit dim nn ne nb ns ss) =
  withString nam $ \str -> do
    let nd = fromEnum dim
    c'ex_put_init exo str (fi nd) (fi nn) (fi ne) (fi nb) (fi ns) (fi ss)


-- ** Coordinate settings.
------------------------------------------------------------------------------
-- TODO: Build a more efficient version.
ex_put_coord_2d :: Exo -> Vector (V2 Double) -> IO CInt
ex_put_coord_2d exo vs = do
  let xs = Vec.toList $ Vec.map (^._x) vs
      ys = Vec.toList $ Vec.map (^._y) vs
      zs = []
  ex_put_coord exo xs ys zs

-- TODO: Build a more efficient version.
ex_put_coord_3d :: Exo -> Vector (V3 Double) -> IO CInt
ex_put_coord_3d exo vs = do
  let xs = Vec.toList $ Vec.map (^._x) vs
      ys = Vec.toList $ Vec.map (^._y) vs
      zs = Vec.toList $ Vec.map (^._z) vs
  ex_put_coord exo xs ys zs

ex_put_coord :: Exo -> [Double] -> [Double] -> [Double] -> IO CInt
ex_put_coord exo xs ys zs = do
  withArray xs $ \p -> do
    withArray ys $ \q -> do
      withArray zs $ \r -> do
        c'ex_put_coord exo (castPtr p) (castPtr q) (castPtr r)

ex_put_coord_names :: CanCString s => Exo -> s -> s -> s -> IO CInt
ex_put_coord_names exo xs ys zs = do
  ns <- mapM toCString [xs, ys, zs]
  withArray ns $ \s -> do
    c'ex_put_coord_names exo s


-- ** Maps and their properties.
------------------------------------------------------------------------------
ex_put_map :: Exo -> Vector CInt -> IO CInt
ex_put_map exo em = Vec.unsafeWith em $ \p -> c'ex_put_map exo p

ex_put_prop_names :: CanCString s => Exo -> ExET -> [s] -> IO CInt
ex_put_prop_names exo eet ns = do
  nx <- mapM toCString ns
  withArray nx $ \ptr -> do
    c'ex_put_prop_names exo eet (fi (length ns)) ptr

-- | For the given entity-type and entity-ID, associate it with the given new
--   entity label and property value.
--   NOTE: in general a value of `0` means that the object does not have this
--     property, whereas a `1` if it does.
ex_put_prop :: CanCString s => Exo -> ExET -> ExID -> s -> CInt -> IO CInt
ex_put_prop exo eet eid str ejd =
  withString str $ \s -> c'ex_put_prop exo eet eid s ejd

-- | Associate the contents of the array with each of the (previously defined)
--   properties (of the given type).
ex_put_prop_array :: CanCString s =>
                     Exo -> ExET -> s -> Vector CInt -> IO CInt
ex_put_prop_array exo eet lab vs =
  withString lab $ \s -> do
    Vec.unsafeWith vs $ \p -> c'ex_put_prop_array exo eet s p


-- ** Element settings.
------------------------------------------------------------------------------
ex_put_elem_block :: CanCString s =>
                     Exo -> ExID -> s -> Int -> Int -> Int -> IO CInt
ex_put_elem_block exo eid typ ne nn na =
  withString typ $ \str ->
    c'ex_put_elem_block exo eid str (fi ne) (fi nn) (fi na)

-- | Define the elements nodes by giving a vector of the nodes that are
--   connected together, to form the element.
ex_put_elem_conn :: Exo -> ExID -> Vector CInt -> IO CInt
ex_put_elem_conn exo eid ns =
  Vec.unsafeWith ns $ \p -> c'ex_put_elem_conn exo eid p

-- | Set the attributes for the given element-block.
ex_put_elem_attr :: Storable a => Exo -> ExID -> Vector a -> IO CInt
ex_put_elem_attr exo eid vs =
  Vec.unsafeWith vs $ \p -> c'ex_put_elem_attr exo eid (castPtr p)


-- ** Node sets.
------------------------------------------------------------------------------
-- | Set up the parameters of a node-set (for the given ID).
ex_put_node_set_param :: Exo -> ExID -> Int -> Int -> IO CInt
ex_put_node_set_param exo eid ns nd =
  c'ex_put_node_set_param exo eid (fi ns) (fi nd)

ex_put_node_set :: Exo -> ExID -> Vector CInt -> IO CInt
ex_put_node_set exo eid ns =
  Vec.unsafeWith ns $ \p -> c'ex_put_node_set exo eid p

ex_put_node_set_dist_fact :: Storable a => Exo -> ExID -> Vector a -> IO CInt
ex_put_node_set_dist_fact exo eid vs =
  Vec.unsafeWith vs $ \p -> c'ex_put_node_set_dist_fact exo eid (castPtr p)


-- ** Variables and parameters.
------------------------------------------------------------------------------
ex_put_var_param :: CanCString s => Exo -> s -> Int -> IO CInt
ex_put_var_param exo lab n =
  withString lab $ \s -> c'ex_put_var_param exo s (fi n)

ex_put_var_names :: CanCString s => Exo -> s -> Int -> [s] -> IO CInt
ex_put_var_names exo lab n ns = withString lab $ \s -> do
  mapM toCString ns >>= withArray `flip` c'ex_put_var_names exo s (fi n)

ex_put_glob_vars :: Storable a => Exo -> Int -> Vector a -> IO CInt
ex_put_glob_vars exo t vs =
  Vec.unsafeWith vs $ \p -> c'ex_put_glob_vars exo (fi t) n (castPtr p)
  where
    n = fi $ Vec.length vs

ex_put_nodal_var :: Storable a => Exo -> Int -> Int -> Vector a -> IO CInt
ex_put_nodal_var exo t i vs = Vec.unsafeWith vs $ \p -> do
  let n = fi $ Vec.length vs
  c'ex_put_nodal_var exo (fi t) (fi i) n (castPtr p)

ex_put_elem_var :: Storable a =>
                   Exo -> Int -> Int -> ExID -> Vector a -> IO CInt
ex_put_elem_var exo t i eid vs = Vec.unsafeWith vs $ \p -> do
  let n = fi $ Vec.length vs
  c'ex_put_elem_var exo (fi t) (fi i) eid n (castPtr p)


-- ** Time-stepping functions.
------------------------------------------------------------------------------
ex_put_time :: Storable a => Exo -> Int -> a -> IO CInt
ex_put_time exo i t = with t $ \p -> c'ex_put_time exo (fi i) (castPtr p)


-- * Internal, helper functions.
------------------------------------------------------------------------------
fi :: (Integral i, Num a) => i -> a
{-# INLINE fi #-}
fi  = fromIntegral
