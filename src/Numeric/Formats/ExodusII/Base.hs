{-# LANGUAGE CPP, ForeignFunctionInterface, TemplateHaskell, QuasiQuotes,
             OverloadedStrings, FlexibleInstances, FlexibleContexts,
             TypeSynonymInstances, GeneralizedNewtypeDeriving, DeriveGeneric,
             RankNTypes, GADTs
  #-}

------------------------------------------------------------------------------
-- |
-- Module      : Numeric.Formats.ExodusII.Base
-- Copyright   : (C) Patrick Suggate 2016
-- License     : BSD
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Haskell ExodusII exporter.
-- 
-- Changelog:
--  + 12/09/2016  -- initial file;
-- 
-- TODO:
--  + higher-level interface;
-- 
------------------------------------------------------------------------------

module Numeric.Formats.ExodusII.Base
  ( -- ExII monad:
    ExII (..)
  , exII
  , runExII
    -- Convenience types:
  , CInt
  , Title
  , Label
    -- Polymorphic writers:
  , ExIICreate(..)
  , x2nvars
  , x2evars
    -- Time and data-frame control:
  , x2gettime
  , x2settime
    -- Internal types and functions:
  , ExIIstate (..)
  , ex2file
  , ex2name
  , ex2vals
  , ex2guid
  , ex2time
  , ex2emsg
  , nnodes
  , nelems
  , neblks
  , x2guid
  , x2fail
  , s2tell
  , ndims
  , nnsets
    -- Error messages:
  , errMsgs
  , dbg
  ) where

import Prelude hiding (id, (.))
import GHC.Generics (Generic)
import Foreign.Ptr
import Foreign.C.Types
import Foreign.C.String
import Foreign.Marshal.Utils (with)
import Foreign.Marshal.Array

import Control.Category
import Control.Monad.IO.Class
import Control.Monad.Trans.State
import Control.Lens (makeLenses, (^.), (%~), (.~))
import Data.Bool
import Data.Vector.Storable (Vector, Storable)
import qualified Data.Vector.Storable as Vec
import qualified Data.ByteString.Char8 as BS
import Linear
import Text.Printf
import Data.Text (pack)
import Control.Logging

import Numeric.Formats.ExodusII.Types
import Numeric.Formats.ExodusII.CAPI


-- * Convenience aliases.
------------------------------------------------------------------------------
type Title    = BS.ByteString
type Label    = BS.ByteString
type Error    = BS.ByteString
type MeshInfo = BS.ByteString

-- | State variable for the `ExII` monad.
data ExIIstate =
  ExIIstate { _ex2file :: Exo
            , _ex2name :: Title
            , _ex2vals :: ExoInit
            , _ex2guid :: ExID
            , _ex2time :: Int
            , _ex2emsg :: Maybe Error
            } deriving (Generic)

newtype ExII m a = ExII { unExII :: StateT ExIIstate m a }
  deriving (Functor, Applicative, Monad, MonadIO, Generic)


-- * ExodusII writer classes.
------------------------------------------------------------------------------
-- | Create initialisation data.
class ExIICreate a where
  x2create :: a -> ExoInit
  x2title  :: a -> Title
  x2minfo  :: a -> MeshInfo


-- * Lenses
------------------------------------------------------------------------------
makeLenses ''ExIIstate


-- * Monad execution functionality.
------------------------------------------------------------------------------
-- | Evaluate the given monadic actions within an @ExII@ context.
runExII :: ExIIstate -> ExII m a -> m (a, ExIIstate)
runExII s k = unExII k `runStateT` s

------------------------------------------------------------------------------
-- | Evaluate the given monadic actions within an @ExII@ context, and generate
--   an output file.
exII ::
     MonadIO m
  => FilePath
  -> Title
  -> ExoInit
  -> ExII m a
  -> m (Either Error a)
exII fp tx xi k = do
  exo <- liftIO $ do
    -- Initialise a new ExodusII file:
    exo <- ex_create fp ExoCreateDouble
    ex_init exo tx xi
    return exo
  let s = ExIIstate exo tx xi (ExID 1) 0 Nothing
  (x, s') <- runExII s k
  liftIO $ do
    ex_update exo
    ex_close  exo

  -- Has a valid ExII file been written?
  case s'^.ex2emsg of
   Just  e -> return $ Left e
   Nothing -> case s'^.ex2vals of
     ExoInit _ 0 0 0 0 0 -> return $ Right x
     _ -> return $ Left "ERROR: not all data has been written."


-- * Helper functions.
------------------------------------------------------------------------------
nnodes :: ExIIstate -> Int
nnodes  = _exNumNodes . _ex2vals

nelems :: ExIIstate -> Int
nelems  = _exNumElems . _ex2vals

nnsets :: ExIIstate -> Int
nnsets  = _exNumNSets . _ex2vals

neblks :: ExIIstate -> Int
neblks  = _exNumEBlks . _ex2vals

ndims  :: ExIIstate -> Int
ndims   = succ . fromEnum . _exNumDims . _ex2vals

------------------------------------------------------------------------------
-- | Error codes and messages.
errMsgs :: [String]
errMsgs  =
  [ "Incorrect number of coordinates (%d given, %d required)." -- 0
  , "Incorrect number of nodes."                               -- 1
  , "Incorrect number of elements (%d given, %d required)."    -- 2
  , "Nodes must be set first (%d remain unset)."               -- 3
  , "Incorrect number of elements (%d given, %d required)."    -- 4
  ]

-- ** Internal ExII helper functions.
------------------------------------------------------------------------------
x2tick :: Monad m => ExII m Int
x2tick  = x2fail (-1) $ ExII $ do
  st <- get
  let t = st^.ex2time + 1
  (put <<< ex2time .~ t) st >> return t

x2guid :: Monad m => ExII m ExID
x2guid  = x2fail (ExID 0) $ ExII $ do
  st <- get
  let i = succ $ st^.ex2guid
  (put <<< ex2guid .~ i) st >> return i

x2fail :: Monad m => a -> ExII m a -> ExII m a
x2fail x k = ExII get >>= maybe k (const $ pure x) . _ex2emsg

x2tell :: Monad m => Maybe Error -> ExII m ()
x2tell Nothing = return ()
x2tell err     = ExII $ get >>= (put <<< ex2emsg .~ err)

s2tell :: Monad m => Maybe Error -> StateT ExIIstate m ()
s2tell e = unExII $ x2tell e

x2file :: Monad m => ExII m Exo
x2file  = ExII $ fmap _ex2file get
{-# INLINE x2file #-}


-- ** Helper-functions for the instances.
------------------------------------------------------------------------------
-- | Set the node-variables.
x2nvars :: MonadIO m => [Label] -> ExII m [ExID]
x2nvars ls = x2fail [] $ ExII $ do
  let nt = "n" :: Label
      vn = length ls
      ix = take vn [ExID 1..]
  exo <- unExII x2file
  res <- liftIO $ do
    ex_put_var_param exo nt vn
    ex_put_var_names exo nt vn ls
  return $ bool [] ix (res == 0)

-- | Set the element-variables.
x2evars :: MonadIO m => [Label] -> ExII m [ExID]
x2evars ls = x2fail [] $ ExII $ do
  let et = "e" :: Label
      en = length ls
      ix = take en [ExID 1..]
  exo <- unExII x2file
  res <- liftIO $ do
    ex_put_var_param exo et en
    ex_put_var_names exo et en ls
  return $ bool [] ix (res == 0)

-- ** Logging helpers
------------------------------------------------------------------------------
dbg :: MonadIO m => String -> m ()
dbg  = debug' . pack
{-# INLINE dbg #-}


-- * Results/variables functions.
------------------------------------------------------------------------------
-- | Set the current time, creating a new results-frame.
x2settime :: MonadIO m => Double -> ExII m Int
x2settime t = x2fail (-1) $ do
  idx <- x2tick
  exo <- x2file
  res <- liftIO $ ex_put_time exo idx t
  bool (x2tell (Just "ERROR: Invalid time.") >> pure (-1)) (pure idx) (res == 0)

x2gettime :: Monad m => ExII m Int
x2gettime  = x2fail (-1) $ ExII $ fmap _ex2time get
