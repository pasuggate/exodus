{-# LANGUAGE CPP #-}
#include <exodusII.h>

module Numeric.Formats.ExodusII.Consts where

import Numeric.Formats.ExodusII.Types


pattern EX'ELEM_BLOCK = ExET EX_ELEM_BLOCK
-- pattern EX'ELEM_BLOCK = ExET c'EX_ELEM_BLOCK
