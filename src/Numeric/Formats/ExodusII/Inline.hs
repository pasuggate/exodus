{-# LANGUAGE ForeignFunctionInterface #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE OverloadedStrings #-}
module Numeric.Formats.ExodusII.Inline where

import Foreign
import Foreign.C
import Foreign.C.Types
import Foreign.C.String
import Data.Map.Maps

import Data.NetCDF
import Data.NetCDF.Utils (NcIO(..))
import qualified Data.NetCDF as NCDF
import qualified Language.C.Inline as C

C.include "<stdio.h>"
C.include "<math.h>"
C.include "<exodusII.h>"

-- #include <stdio.h>
-- #include <netcdf.h>
-- #include <exodusII.h>


type Title = String
type ExRd  = NCDF.NcInfo NCDF.NcRead
type ExWr  = NCDF.NcInfo NCDF.NcWrite

newtype ExMode = ExMode CInt

-- pattern ExClobber = ExMode EX_CLOBBER


-- | Setup a new file for writing.
ex_setup :: FilePath -> Title -> NcIO (ExWr)
ex_setup fp ts = do
--   let nc = NCDF.NcInfo fp bare bare bare NCDF.ncInvalidId bare
  let nc = NCDF.emptyNcInfo fp
  NCDF.createFile nc

ex_create :: CString -> CInt -> IO (Maybe CInt)
ex_create fp mode = do
  let size = 8 :: CInt
  r <- with size $ \p -> do
    [C.exp| int{ ex_create($(char *fp), $(int mode), $(int *p), $(int *p)) } |]
  if r < 0 then return Nothing else return (Just size)


myprint :: IO ()
myprint = do
  x <- [C.exp| int{ printf("Some number: %.2f\n", cos(0.5)) } |]
  putStrLn $ show x ++ " characters printed."
