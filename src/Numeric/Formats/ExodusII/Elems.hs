{-# LANGUAGE CPP, ForeignFunctionInterface, TemplateHaskell, QuasiQuotes,
             OverloadedStrings, FlexibleInstances, FlexibleContexts,
             TypeSynonymInstances, GeneralizedNewtypeDeriving, DeriveGeneric,
             RankNTypes, GADTs, DefaultSignatures
  #-}

------------------------------------------------------------------------------
-- |
-- Module      : Numeric.Formats.ExodusII.Elems
-- Copyright   : (C) Patrick Suggate 2016
-- License     : BSD
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Writes elements and element-blocks, for the Haskell ExodusII exporter.
-- 
-- Changelog:
--  + 12/09/2016  -- initial file;
--  + 02/02/2020  -- refactored the elements stuff into separate module;
-- 
------------------------------------------------------------------------------

module Numeric.Formats.ExodusII.Elems
       ( ExIIElems   (..)
       , ExIIEBlocks (..)
       , ElemBlock   (..)
       , Ex2EBlock   (..)
       , Quads
       , writeBlock'
       , writeBlock
       ) where

import Prelude hiding (id, (.))
import GHC.Generics (Generic)
import Foreign.Ptr
import Foreign.C.Types
import Foreign.C.String
import Foreign.Marshal.Utils (with)
import Foreign.Marshal.Array

import Control.Category
import Control.Monad (when, unless)
import Control.Monad.IO.Class
import Control.Monad.Trans.State
import Control.Lens (makeLenses, (^.), (%~), (.~))
import Data.Vector.Storable (Vector, Storable)
import qualified Data.Vector.Storable as Vec
import qualified Data.ByteString.Char8 as BS
import Linear (V4 (..))
import Text.Printf
import Data.Text (pack)
import Control.Logging

import Numeric.Formats.ExodusII.Types
import Numeric.Formats.ExodusII.CAPI
import Numeric.Formats.ExodusII.Base


-- * ExodusII element-writer classes.
------------------------------------------------------------------------------
-- | Extract element-blocks from a mesh data type.
class ExIIEBlocks a where
  x2nblocks :: a -> Int
  x2eblocks :: a -> [Ex2EBlock]
  x2ebprops :: a -> [Label]
  x2ebprops  = const []         -- ^ default method, if no named properties

------------------------------------------------------------------------------
-- | Getters for any type that can represent an element-block.
class ElemBlock a where
--   blockId  :: a -> Int
  numElems :: a -> Int
  nodesPer :: a -> Int
  elemType :: a -> Label
  attrsPer :: a -> Int
  blkProps :: a -> [Label]
  elemConn :: a -> Vector CInt
  elemAttr :: a -> Vector Double

------------------------------------------------------------------------------
-- | Write element blocks.
class ExIIElems a where
  x2elems  :: MonadIO m => a -> ExII m ()
  default x2elems :: (MonadIO m, ExIIEBlocks a) => a -> ExII m ()
  x2elems mesh = ExII $ do
    exo <- _ex2file <$> get
    let bs = [ExID 1..] `zip` x2eblocks mesh
        ps = x2ebprops mesh
        eb = toEnum c'EX_ELEM_BLOCK
    -- do the element-blocks have named properties?
    unless (null ps) $ liftIO $ do
      dbg "writing element-property names"
      ex_put_prop_names exo eb ps >> pure ()
    -- write each element-block:
    mapM_ `flip` bs $ \(i, Ex2EBlock b) -> do
      dbg $ printf "writing element-block: %s" (show i)
      liftIO $ writeBlock exo i b
      modify $ ex2vals %~ (exNumElems %~ (\x -> x - numElems b) >>>
                           exNumEBlks %~ pred)

{-- }
    modify $ ex2vals %~ (exNumEBlks %~ (\x -> x - length bs) >>>
                         exNumElems %~ 
    -- update the counts for the number of remaining elements and blocks to be
    -- written:
    modify (ex2vals %~ (exNumEBlks %~ (+negate nb) <<< exNumElems %~ (+negate ne))
--}


-- * Data-types for exporting to ExodusII files
------------------------------------------------------------------------------
-- | Element-block wrapper so that generic instances can be used for writing
--   element-block data.
data Ex2EBlock where
  Ex2EBlock :: forall a. ElemBlock a => a -> Ex2EBlock

-- ** Convenience aliases
------------------------------------------------------------------------------
type Quads = Vector (V4 CInt)


-- * Lenses and instances
------------------------------------------------------------------------------
-- | Write element sets.
--   
--   TODO: Support element attributes?
instance ExIIElems [(Label, Quads)] where
  x2elems es = x2fail () $ ExII $ do
    st <- get
    let (ne, nb) = (sum $ Vec.length . snd <$> es, length es)
        ls = fst $ unzip es
        qt = "QUAD" :: BS.ByteString
    if nnodes st == 0 && nelems st >= ne && neblks st >= nb
      then (do let exo = st^.ex2file
                   st' = ex2vals %~ (exNumEBlks %~ (\x -> x - nb) <<<
                                     exNumElems %~ (\x -> x - ne)) $ st
                   em  = Vec.enumFromN (fi $ neblks st - nb + 1) nb :: Vector CInt
                   eb  = toEnum c'EX_ELEM_BLOCK
                   ez  = [ExID 1..] `zip` es
               liftIO $ ex_put_map exo em
               -- ^ setup the property-names:
               liftIO $ ex_put_prop_names exo eb ls
               -- ^ Set up the element sets:
               flip mapM_ ez $ \(i, (l, e)) -> do
                 qi <- unExII x2guid
                 liftIO $ do
                   ex_put_elem_block exo qi qt (Vec.length e) 4 0
                   ex_put_prop       exo eb qi l 1
                   ex_put_elem_conn  exo qi $ Vec.unsafeCast e
--                    ex_put_elem_attr  exo qi a -- TODO: attributes?
               -- ^ Update the state:
               put st' >> return ())
      else let erE = printf (errMsgs!!2) ne (nelems st)
               erN = printf (errMsgs!!3) (nnodes st)
               erB = printf (errMsgs!!4) nb (neblks st)
               err = case (nnodes st == 0, nelems st >= ne) of
                 (False, _) -> erN
                 (_, False) -> erE
                 _          -> erB
           in  s2tell $ Just $ BS.pack err


-- * Helper functions
------------------------------------------------------------------------------
writeBlock' :: Exo -> ExID -> Ex2EBlock -> IO ()
writeBlock' exo i (Ex2EBlock b) = writeBlock exo i b

writeBlock :: ElemBlock a => Exo -> ExID -> a -> IO ()
writeBlock exo i b = do
  let eb = toEnum c'EX_ELEM_BLOCK
  ex_put_elem_block exo i (elemType b) (numElems b) (nodesPer b) (attrsPer b)
  dbg "setting element-block parameters:"
  dbg $ printf " + number of elements: %d" (numElems b)
  dbg $ printf " + nodes per element:  %d" (nodesPer b)
  dbg $ printf " + attrs per element:  %d" (attrsPer b)
  ex_put_elem_conn  exo i (elemConn b)
  dbg "writing element connectivities:"
  dbg $ show (elemConn b)
  (\q -> ex_put_prop exo eb i q 1) `mapM_` blkProps b
  when (attrsPer b > 0) $ do
    dbg "writing element-block attributes"
    ex_put_elem_attr exo i (elemAttr b) >> pure ()
{-# INLINE writeBlock #-}
