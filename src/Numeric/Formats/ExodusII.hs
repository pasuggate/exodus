{-# LANGUAGE FlexibleInstances, UndecidableInstances, InstanceSigs,
             OverloadedStrings
  #-}

------------------------------------------------------------------------------
-- |
-- Module      : Numeric.Formats.ExodusII
-- Copyright   : (C) Patrick Suggate 2016
-- License     : BSD
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Haskell ExodusII exporter.
-- 
-- Changelog:
--  + 17/07/2016  -- initial file;
-- 
-- TODO:
--  + higher-level interface;
-- 
------------------------------------------------------------------------------

module Numeric.Formats.ExodusII
       ( module Numeric.Formats.ExodusII.Types
       , module Numeric.Formats.ExodusII.Base
       , module Numeric.Formats.ExodusII.Nodes
       , module Numeric.Formats.ExodusII.Elems
       , module Numeric.Formats.ExodusII.Steps
--        , module Numeric.Formats.ExodusII.CAPI
       , ExodusII (..)
       , saveExII
         -- Testing and examples
       , testExo
       , testBaseExo
       ) where

import Foreign.Ptr
import Foreign.C.Types
import Foreign.C.String
import Foreign.Marshal.Utils (with)
import Foreign.Marshal.Array
import System.FilePath.Posix (dropExtension, (<.>))
import Control.Monad.IO.Class
import Control.Lens (makeLenses, (^.))
import Data.Vector.Storable (Vector, Storable)
import qualified Data.Vector.Storable as Vec
import qualified Data.ByteString.Char8 as BS
import Linear
import Text.Printf
-- import Data.Text (pack)
import Control.Logging

import Numeric.Formats.ExodusII.Types
-- import Numeric.Formats.ExodusII.Bindings
import Numeric.Formats.ExodusII.CAPI
import Numeric.Formats.ExodusII.Base
import Numeric.Formats.ExodusII.Nodes
import Numeric.Formats.ExodusII.Elems
import Numeric.Formats.ExodusII.Steps


-- * Exodus import/export type-classes.
------------------------------------------------------------------------------
class ExodusII a where
  toExII :: FilePath -> a -> IO ()


-- * Some instances
------------------------------------------------------------------------------
instance ( ExIICreate a
         , ExIICoords a
         , ExIIElems  a
         , ExIINodes  a
         , ExIITSteps a
         ) => ExodusII a where
  toExII :: FilePath -> a -> IO ()
  toExII  = saveExII
  {-# INLINE toExII #-}


-- * Generic save function
------------------------------------------------------------------------------
-- | Default instance for any type that meets certain requirements.
saveExII ::
     ExIICreate a
  => ExIICoords a
  => ExIIElems  a
  => ExIINodes  a
  => ExIITSteps a
  => FilePath
  -> a
  -> IO ()
saveExII fp mesh = withFileLogging (dropExtension fp <.> "log") $ do
  let exi = x2create mesh
  dbg $ printf "Creating new ExodusII file: %s" (show exi)
  res <- exII fp (x2title mesh) exi $ do
    x2coords mesh
    x2elems  mesh
    x2nodes  mesh
    x2tsteps mesh
  case res of
    Left err -> putStrLn $ BS.unpack err
    _        -> return ()


-- * Exodus testing & examples.
------------------------------------------------------------------------------
-- | Low-level example.
testExo :: IO CInt
testExo  = do
  let vs = Vec.fromList [ V3 2 0 0, V3 4 0 0, V3 3 1 0
                        , V3 0 0 0, V3 1 0 0, V3 1 1 0, V3 0 1 0
                        ] :: Vector (V3 Double)
      es = [Vec.fromList [1, 2, 3], Vec.fromList [4, 5, 6, 7] :: Vector CInt]
      em = Vec.enumFromN 1 (length es) :: Vector CInt
      tx = "flabby" :: BS.ByteString
      fp = "test.exo" :: BS.ByteString
      (ti, ts, tt) = (toEnum 10 :: ExID, toEnum 20 :: ExID, "TRI"  :: BS.ByteString)
      (qi, qs, qt) = (toEnum 11 :: ExID, toEnum 21 :: ExID, "QUAD" :: BS.ByteString)
      pn = ["TOP", "RIGHT" :: BS.ByteString]
      fs = "FACE" :: BS.ByteString
      ax = map Vec.singleton [pi :: Double, pi+3]
      vx = "VELOCITY" :: BS.ByteString

  -- ^ Initialise a new ExodusII file:
  exo <- ex_create fp ExoCreateDouble
  ex_init exo tx $ ExoInit Exo3D (Vec.length vs) (length es) 2 2 0

  -- ^ Set the coordinates:
  ex_put_coord_3d exo vs
  ex_put_coord_names exo "xcoor" "ycoor" ("zcoor" :: BS.ByteString)

  -- ^ Set up the elements:
  ex_put_map exo em
  ex_put_elem_block exo ti tt 1 3 1 -- #elems, #nodes/elem, #attrs/elem
  ex_put_elem_block exo qi qt 1 4 1
  ex_put_prop_names exo (toEnum c'EX_ELEM_BLOCK) pn
  ex_put_prop exo (toEnum c'EX_ELEM_BLOCK) ti (last pn) (toEnum 1)
  ex_put_prop exo (toEnum c'EX_ELEM_BLOCK) qi (head pn) (toEnum 1)
  ex_put_elem_conn exo ti $ es!!0
  ex_put_elem_conn exo qi $ es!!1
  ex_put_elem_attr exo ti $ ax!!0
  ex_put_elem_attr exo qi $ ax!!1

  -- ^ Now set up the node sets:
  ex_put_node_set_param exo ts 3 3 >>= print
  ex_put_node_set exo ts (Vec.enumFromN 1 3) >>= print
  ex_put_node_set_dist_fact exo ts (Vec.enumFromN 1 3 :: Vector Double) >>= print
--   ex_put_node_set_param exo ts 5 5 >>= print
--   ex_put_node_set exo ts (Vec.enumFromN 100 5) >>= print
--   ex_put_node_set_dist_fact exo ts (Vec.enumFromN 1 5 :: Vector Double) >>= print
  ex_put_node_set_param exo qs 3 3
  ex_put_node_set exo qs (Vec.enumFromN 200 3)
  ex_put_node_set_dist_fact exo qs (Vec.enumFromN 1.1 3 :: Vector Double)
  ex_put_prop exo (toEnum c'EX_NODE_SET) ts fs (toEnum 4) >>= print
  ex_put_prop exo (toEnum c'EX_NODE_SET) qs fs (toEnum 5)
  ex_put_prop_array exo (toEnum c'EX_NODE_SET) vx (Vec.fromList [1000, 2000]) >>= print

  -- ^ Set up the global, nodal, and elemental variables:
  let (ng, gv, gx) = (length gx - 1, head gx, ["g", "glo_vars" :: BS.ByteString])
      (nn, nv, nx) = (length nx - 1, head nx, ["n", "nod_var0", "nod_var1" :: BS.ByteString])
      (ne, ev, ex) = (length ex - 1, head ex, ["e", "ele_var0", "ele_var1", "ele_var2" :: BS.ByteString])

  ex_put_var_param exo gv ng
  ex_put_var_names exo gv ng (tail gx)
  ex_put_var_param exo nv nn
  ex_put_var_names exo nv nn (tail nx)
  ex_put_var_param exo ev ne
  ex_put_var_names exo ev ne (tail ex)

  -- ^ Write a single time-step of results:
  flip mapM_ [1] $ \t -> do
    let t' = fi (t-1) / 100 :: Double
    ex_put_time exo t t' >>= print
    flip mapM_ [1..ng] $ \i -> do -- for each global-variable
      ex_put_glob_vars exo t $ Vec.replicate ng (2.73 - 7*t')
    flip mapM_ [1..nn] $ \i -> do -- for each node and nodal-variable
      ex_put_nodal_var exo t i $ Vec.enumFromStepN 0 t' $ Vec.length vs
    flip mapM_ [1..ne] $ \i -> do -- for each element and element-variable
      ex_put_elem_var exo t i ti $ Vec.singleton (1.3 + t')
      ex_put_elem_var exo t i qi $ Vec.singleton (0.3 + 3*t')
    ex_update exo

  -- ^ Close the ExodusII file:
  ex_close exo


-- * Testing and examples.
------------------------------------------------------------------------------
testBaseExo :: IO ()
testBaseExo  = do
  let fp = "flappy.exo"
      vs = Vec.fromList [ V3 0 0 0, V3 1 0 0, V3 1 1 0, V3 0 1 0
                        ] :: Vector (V3 Double)
      vx = vs Vec.++ Vec.map (+ V3 2 0.5 0) vs
--       bx = Vec.enumFromN 1 2 :: Nodes
      e0 = Vec.enumFromN 1 4 :: Nodes
      e1 = Vec.enumFromN 5 4
      ex = [("Mesh" :: Label, Vec.unsafeCast e0 :: Quads), ("Dual", Vec.unsafeCast e1)]
      xx = Vec.reverse vx :: Vector V3R
--       xx = Vec.unsafeCast $ Vec.reverse vx :: Vector Double

      -- construct an ExII action:
      xi = ExoInit Exo3D 8 2 2 2 0
      xk = do
        x2coords  vx
        x2nodes  [e0, e1]
        x2elems   ex
        vi <- x2nvars ["VX", "VY", "VZ"]
        x2settime 0
        x2ndata $ vi `zip` vu3 xx
        return ()

  res <- exII fp "flappy" xi xk
  case res of
    Left err -> print res
    _        -> return ()
